using PruebaGetechnologiesMx.Business;
using PruebaGetechnologiesMx.Repository;
using PruebaGetechnologiesMx.Tools.CustomExeption;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<IDirectorioBusiness, DirectorioBusiness>();
builder.Services.AddScoped<IVentasBusiness, VentasBusiness>();

builder.Services.AddScoped<IPersonaRepository, PersonaRepository>();
builder.Services.AddScoped<IFacturaRepository, FacturaRepository>();

// Add services to the container.

builder.Services.AddControllers();

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseMiddleware<ExeptionMiddleware>();

app.MapControllers();

app.Run();
