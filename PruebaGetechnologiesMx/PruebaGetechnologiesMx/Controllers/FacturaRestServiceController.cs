﻿using Microsoft.AspNetCore.Mvc;
using PruebaGetechnologiesMx.Business;
using PruebaGetechnologiesMx.Model;
using PruebaGetechnologiesMx.Tools.CustomExeption;

namespace PruebaGetechnologiesMx.RestServices.Controllers
{
    [Route("api/[controller]")]
    public class FacturaRestServiceController : ControllerBase
    {
        private readonly ILogger<FacturaRestServiceController> _logger;
        private readonly IVentasBusiness _ventasBusiness;
        public IConfiguration _configuration;

        public FacturaRestServiceController(ILogger<FacturaRestServiceController> logger,
                                       IVentasBusiness ventasBusiness,
                                       IConfiguration configuration)
        {
            _logger = logger;
            _ventasBusiness = ventasBusiness;
            _configuration = configuration;
        }

        [HttpGet]
        [Route("findfacturaByPersona")]
        public ActionResult<List<Factura>> FindfacturaByPersona(int idPersona)
        {
            try
            {
                if (idPersona <= 0)
                    throw new BadRequestException("Favor de ingresar el id persona");

                List<Factura> response = _ventasBusiness.FindFacturasByPersona(idPersona);

                if (response != null)
                    return Ok(response);
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        [HttpPost]
        [Route("storedFactura")]
        public ActionResult<bool> StoredFactura([FromBody] Factura factura)
        {
            try
            {
                if(factura == null)
                    throw new BadRequestException("Peticion incorrecta");

                bool response = _ventasBusiness.StoredFactura(factura);

                if (response)
                    return Ok(response);
                else
                    return StatusCode(500, "Error al guardar la persona");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

    }
}
