﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PruebaGetechnologiesMx.Business;
using PruebaGetechnologiesMx.Model;
using PruebaGetechnologiesMx.Tools.CustomExeption;
using System.Collections.Generic;

namespace PruebaGetechnologiesMx.RestServices.Controllers
{
    [Route("api/[controller]")]
    public class DirectorioRestServiceController : ControllerBase
    {

        private readonly ILogger<DirectorioRestServiceController> _logger;
        private readonly IDirectorioBusiness _directorioBusiness;
        public IConfiguration _configuration;

        public DirectorioRestServiceController(ILogger<DirectorioRestServiceController> logger,
                                       IDirectorioBusiness directorioBusiness,
                                       IConfiguration configuration)
        {
            _logger = logger;
            _directorioBusiness = directorioBusiness;
            _configuration = configuration;
        }

        [HttpGet]
        [Route("findPersonas")]
        public ActionResult<List<Persona>> FindPersonas()
        {
            try
            {
                List<Persona> response = _directorioBusiness.FindPersonas();

                if (response?.Count() > 0)
                    return Ok(response);
                else
                    return NotFound();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        [Route("findPersonaByIdentificacion")]
        public ActionResult<Persona> FindPersonaByIdentificacion(string identificacion)
        {
            try
            {
                if (string.IsNullOrEmpty(identificacion))
                    throw new BadRequestException("Favor de ingresar una identificacion");

                Persona response = _directorioBusiness.FindPersonaByIdentificacion(identificacion);

                if (response != null)
                    return Ok(response);
                else
                    return NotFound();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpDelete]
        [Route("deletePersonaByIdentificacion")]
        public ActionResult<bool> DeletePersonaByIdentificacion(string identificacion)
        {
            try
            {
                if (string.IsNullOrEmpty(identificacion))
                    throw new BadRequestException("Favor de ingresar una identificacion");

                bool response = _directorioBusiness.DeletePersonaByIdentificacion(identificacion);

                if (response)
                    return Ok(response);
                else
                    return NotFound();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("storedPersona")]
        public ActionResult<bool> StoredPersona([FromBody] Persona persona)
        {
            try
            {
                if (string.IsNullOrEmpty(persona.Nombre))
                    throw new BadRequestException("Favor de ingresar un nombre");

                if (string.IsNullOrEmpty(persona.ApellidoPaterno))
                    throw new BadRequestException("Favor de ingresar un apellido paterno");

                if (string.IsNullOrEmpty(persona.Identificacion))
                    throw new BadRequestException("Favor de ingresar una identificacion");


                bool response = _directorioBusiness.StoredPersona(persona);

                if (response)
                    return Ok(response);
                else
                    return StatusCode(500, "Error al guardar la persona");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
