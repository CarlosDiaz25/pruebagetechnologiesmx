﻿namespace PruebaGetechnologiesMx.Model
{
    public class Factura
    {
        public Factura() 
        {
            Fecha = DateTime.Now.ToString();
            Monto = 0;
            IdPersona = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Fecha { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Monto { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IdPersona { get; set; }
    }
}