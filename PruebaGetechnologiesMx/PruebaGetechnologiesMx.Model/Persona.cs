﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaGetechnologiesMx.Model
{
    public class Persona
    {
        public Persona()
        {
            Nombre = "";
            ApellidoPaterno = "";
            ApellidoMaterno = "";
            Identificacion = "";
            Facturas = new();
        }


        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ApellidoPaterno { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ApellidoMaterno { get; set; }

        /// <summary>
        /// Identificacion oficial de la INE
        /// </summary>
        public string Identificacion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<Factura> Facturas { get; set; }


    }
}
