﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PruebaGetechnologiesMx.Model;
using PruebaGetechnologiesMx.Repository;
using System.Data.Common;
using System.Data.SQLite;
using System.Data;
using PruebaGetechnologiesMx.Tools.CustomExeption;
using System.Text.RegularExpressions;

namespace PruebaGetechnologiesMx.Business
{
    public class DirectorioBusiness : IDirectorioBusiness
    {
        private readonly ILogger<DirectorioBusiness> _logger;
        private readonly IPersonaRepository _personaRepository;
        private readonly IFacturaRepository _facturaRepository;
        public IConfiguration _configuration;

        public DirectorioBusiness(ILogger<DirectorioBusiness> logger,
                                  IConfiguration configuration,
                                  IPersonaRepository personaRepository,
                                  IFacturaRepository facturaRepository)
        {
            _personaRepository = personaRepository;
            _facturaRepository = facturaRepository;
            _logger = logger;
            _configuration = configuration;
        }


        public List<Persona> FindPersonas()
        {
            try
            {
               return _personaRepository.FindPersonas();
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw;
            }
        }

        public Persona FindPersonaByIdentificacion(string identificacion)
        {
            try
            {
                return _personaRepository.FindPersonaByIdentificacion(identificacion);
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw;
            }
        }

        public bool DeletePersonaByIdentificacion(string identificacion)
        {
            try
            {
                if (!_personaRepository.ValidarExistenciaIdentificacion(identificacion))
                    throw new BadRequestException("la identificacion no existe");

                var persona = _personaRepository.FindPersonaByIdentificacion(identificacion);

                _facturaRepository.DeleteFacturasByPersona(persona.Id);

                return _personaRepository.DeletePersonaByIdentificacion(identificacion);
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw;
            }
        }

        public bool StoredPersona(Persona persona)
        {
            try
            {
                if (!Regex.IsMatch(persona.Nombre, @"^[A-Z][a-zA-Z ]*$"))
                    throw new BadRequestException("El nombre contiene caracteres no validos");

                if (!Regex.IsMatch(persona.ApellidoPaterno, @"^[A-Z][a-zA-Z ]*$"))
                    throw new BadRequestException("El Apellido Paterno contiene caracteres no validos");

                if (!Regex.IsMatch(persona.Identificacion, @"^[1-9]+$"))
                    throw new BadRequestException("la identificacion contiene caracteres no validos");

                if(_personaRepository.ValidarExistenciaIdentificacion(persona.Identificacion))
                    throw new BadRequestException("la identificacion ya existe");

                return _personaRepository.StoredPersona(persona);
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw;
            }
        }
    }
}