﻿using PruebaGetechnologiesMx.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaGetechnologiesMx.Business
{
    public interface IVentasBusiness
    {
        List<Factura> FindFacturasByPersona(int idPersona);
        bool StoredFactura(Factura factura);
    }
}
