﻿using PruebaGetechnologiesMx.Model;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PruebaGetechnologiesMx.Repository;
using PruebaGetechnologiesMx.Tools.CustomExeption;

namespace PruebaGetechnologiesMx.Business
{
    public class VentasBusiness : IVentasBusiness
    {
        private readonly ILogger<VentasBusiness> _logger;
        private readonly IFacturaRepository _facturaRepository;
        private readonly IPersonaRepository _personaRepository;
        public IConfiguration _configuration;

        public VentasBusiness(ILogger<VentasBusiness> logger,
                              IConfiguration configuration,
                              IFacturaRepository facturaRepository,
                              IPersonaRepository personaRepository)
        {
            _facturaRepository = facturaRepository;
            _personaRepository = personaRepository;
            _logger = logger;
            _configuration = configuration;
        }

        public List<Factura> FindFacturasByPersona(int idPersona)
        {
            try
            {
                if (_personaRepository.FindPersonaById(idPersona) == null)
                    throw new BadRequestException("La persona no existe");

                return _facturaRepository.FindFacturasByPersona(idPersona);
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw;
            }
        }

        public bool StoredFactura(Factura factura)
        {
            try
            {
                if(factura.Monto <= 0)
                    throw new BadRequestException("El monto de la factura debe ser mayor a $0");

                if (_personaRepository.FindPersonaById(factura.IdPersona) == null)
                    throw new BadRequestException("La persona no existe");

                return _facturaRepository.StoredFactura(factura);
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw;
            }
        }
    }
}
