﻿namespace PruebaGetechnologiesMx.Tools.CustomExeption
{
    public class NotFoundException : Exception
    {
        public NotFoundException(String mensaje) : base(mensaje)
        {

        }
    }
}
