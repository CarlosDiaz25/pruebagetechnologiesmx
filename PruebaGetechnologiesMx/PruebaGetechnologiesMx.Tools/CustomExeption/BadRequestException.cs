﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PruebaGetechnologiesMx.Tools.CustomExeption
{
    public class BadRequestException : Exception
    {
        public BadRequestException(String mensaje) : base(mensaje)
        {

        }
    }
}
