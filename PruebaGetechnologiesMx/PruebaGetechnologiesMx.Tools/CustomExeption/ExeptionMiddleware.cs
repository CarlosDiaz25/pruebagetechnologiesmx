﻿using Newtonsoft.Json;
using System.Net;
using Microsoft.AspNetCore.Http;

namespace PruebaGetechnologiesMx.Tools.CustomExeption
{
    public class ExeptionMiddleware 
    {
        private readonly RequestDelegate _next;

        public ExeptionMiddleware (RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke (HttpContext httpcontext)
        {
            try
            {
                await _next(httpcontext);
            }
            catch (NotFoundException ex)
            {
                //_logger.LogError(ex,$"Oooops! Algo salio mal : { ex.Message }");
                await HandleNotFoundExceptionAsync(httpcontext, ex);
            }
            catch (NoAutorizationException ex)
            {
                //_logger.LogError(ex,$"Oooops! Algo salio mal : { ex.Message }");
                await HandleNoAutorizationExceptionAsync(httpcontext, ex);
            }
            catch (FormatException ex)
            {
                //_logger.LogError(ex,$"Oooops! Algo salio mal : { ex.Message }");
                await HandleBadRequesExceptionAsync(httpcontext, ex);
            }
            catch (BadRequestException ex)
            {
                //_logger.LogError(ex,$"Oooops! Algo salio mal : { ex.Message }");
                await HandleBadRequesExceptionAsync(httpcontext, ex);
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex,$"Oooops! Algo salio mal : { ex.Message }");
                await HandleGlobalExceptionAsync(httpcontext,ex);
            }
        }

        private static Task HandleGlobalExceptionAsync(HttpContext httpcontext,Exception exception)
        {
            httpcontext.Response.ContentType = "application/json";
            httpcontext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return httpcontext.Response.WriteAsync(JsonConvert.SerializeObject(new ErrorDetails()
            {
                StatusCode = StatusCodes.Status500InternalServerError,
                Message = "Algo salio mal. Error!",
                StackTrace = exception.Message
            }));
        }

        private static Task HandleBadRequesExceptionAsync(HttpContext httpcontext, Exception exception)
        {
            httpcontext.Response.ContentType = "application/json";
            httpcontext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return httpcontext.Response.WriteAsync(JsonConvert.SerializeObject(new ErrorDetails()
            {
                StatusCode = StatusCodes.Status400BadRequest,
                Message = exception.Message,
                StackTrace = exception.StackTrace
            }));
        }

        private static Task HandleNotFoundExceptionAsync(HttpContext httpcontext, Exception exception)
        {
            httpcontext.Response.ContentType = "application/json";
            httpcontext.Response.StatusCode = (int)HttpStatusCode.NotFound;
            return httpcontext.Response.WriteAsync(JsonConvert.SerializeObject(new ErrorDetails()
            {
                StatusCode = StatusCodes.Status404NotFound,
                Message = exception.Message,
                StackTrace = exception.StackTrace
            }));
        }

        private static Task HandleNoAutorizationExceptionAsync(HttpContext httpcontext, Exception exception)
        {
            httpcontext.Response.ContentType = "application/json";
            httpcontext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            return httpcontext.Response.WriteAsync(JsonConvert.SerializeObject(new ErrorDetails()
            {
                StatusCode = StatusCodes.Status401Unauthorized,
                Message = exception.Message,
                StackTrace = exception.StackTrace
            }));
        }
    }
}
