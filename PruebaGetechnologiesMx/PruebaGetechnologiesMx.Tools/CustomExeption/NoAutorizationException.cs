﻿namespace PruebaGetechnologiesMx.Tools.CustomExeption
{
    public class NoAutorizationException : Exception
    {
        public NoAutorizationException(String mensaje) : base(mensaje)
        {

        }
    }
}
