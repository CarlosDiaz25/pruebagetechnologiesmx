﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PruebaGetechnologiesMx.Model;
using System.Data.Common;
using System.Data.SQLite;
using System.Data;

namespace PruebaGetechnologiesMx.Repository
{
    public class FacturaRepository : IFacturaRepository
    {
        private readonly ILogger<FacturaRepository> _logger;
        public IConfiguration _configuration;

        public FacturaRepository(ILogger<FacturaRepository> logger,
                                       IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public List<Factura> FindFacturasByPersona(int idPersona)
        {
            try
            {
                List<Factura> facturas = new();
                using (SQLiteConnection connection = new(_configuration.GetConnectionString("SQLConnServidor")))
                {
                    connection.Open();

                    string query =
                        "SELECT " +
                            "id, " +
                            "fecha, " +
                            "monto " +
                        "FROM Factura " +
                        "WHERE idPersona = @idPersona";

                    SQLiteCommand dbCommand = new(query, connection);
                    dbCommand.Parameters.Add(new SQLiteParameter("@idPersona", idPersona));
                    dbCommand.CommandType = CommandType.Text;

                    using (DbDataReader objReader = dbCommand.ExecuteReader())
                    {
                        while (objReader.Read())
                        {
                            Factura factura = new()
                            {
                                Id = Convert.ToInt32(objReader["id"]),
                                Fecha = objReader["fecha"].ToString(),
                                Monto = Convert.ToDecimal(objReader["monto"]),
                            };
                            facturas.Add(factura);
                        }
                    }
                }
                return facturas;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw new ApplicationException(exp.Message, exp);
            }
        }

        public bool DeleteFacturasByPersona(int idPersona)
        {
            try
            {
                using (SQLiteConnection connection = new(_configuration.GetConnectionString("SQLConnServidor")))
                {
                    connection.Open();

                    string query =
                        "DELETE " +
                        "FROM Factura " +
                        "WHERE idPersona = @idPersona";

                    SQLiteCommand dbCommand = new(query, connection);
                    dbCommand.Parameters.Add(new SQLiteParameter("@idPersona", idPersona));
                    dbCommand.CommandType = CommandType.Text;

                    if (dbCommand.ExecuteNonQuery() < 1)
                        return false;

                    return true;
                }
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw new ApplicationException(exp.Message, exp);
            }
        }

        public bool StoredFactura(Factura factura)
        {
            try
            {
                using (SQLiteConnection connection = new(_configuration.GetConnectionString("SQLConnServidor")))
                {
                    connection.Open();

                    string query =
                        "INSERT INTO Factura (fecha,monto,idPersona) " +
                        "VALUES(@fecha,@monto,@idPersona)";

                    SQLiteCommand dbCommand = new(query, connection);
                    dbCommand.Parameters.Add(new SQLiteParameter("@fecha", factura.Fecha));
                    dbCommand.Parameters.Add(new SQLiteParameter("@monto", factura.Monto));
                    dbCommand.Parameters.Add(new SQLiteParameter("@idPersona", factura.IdPersona));
                    dbCommand.CommandType = CommandType.Text;

                    if (dbCommand.ExecuteNonQuery() < 1)
                        return false;

                    return true;
                }
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw new ApplicationException(exp.Message, exp);
            }
        }
    }
}