﻿using PruebaGetechnologiesMx.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaGetechnologiesMx.Repository
{
    public interface IFacturaRepository
    {
        List<Factura> FindFacturasByPersona(int idPersona);
        bool StoredFactura(Factura factura);

        bool DeleteFacturasByPersona(int identificacion);
    }
}
