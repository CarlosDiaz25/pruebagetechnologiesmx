﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using PruebaGetechnologiesMx.Model;

namespace PruebaGetechnologiesMx.Repository
{
    public class PersonaRepository : IPersonaRepository
    {
        private readonly ILogger<PersonaRepository> _logger;
        public IConfiguration _configuration;

        public PersonaRepository(ILogger<PersonaRepository> logger,
                                       IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public List<Persona> FindPersonas()
        {
            try
            {
                List<Persona> personas = new();
                using (SQLiteConnection connection = new(_configuration.GetConnectionString("SQLConnServidor")))
                {
                    connection.Open();

                    string query =
                        "SELECT " +
                            "id, " +
                            "nombre, " +
                            "apellidoPaterno, " +
                            "apellidoMaterno, " +
                            "identificacion " +
                        "FROM Persona ";

                    SQLiteCommand dbCommand = new(query, connection);
                    dbCommand.CommandType = CommandType.Text;

                    using (SQLiteDataReader objReader = dbCommand.ExecuteReader())
                    {
                        while (objReader.Read())
                        {
                            Persona persona = new()
                            {
                                Id = Convert.ToInt32(objReader["id"]),
                                Nombre = objReader["nombre"].ToString(),
                                ApellidoPaterno = objReader["apellidoPaterno"].ToString(),
                                ApellidoMaterno = objReader["apellidoMaterno"].ToString(),
                                Identificacion = objReader["identificacion"].ToString()
                            };
                            personas.Add(persona);
                        }
                    }
                }
                return personas;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw new ApplicationException(exp.Message, exp);
            }
        }

        public Persona FindPersonaByIdentificacion(string identificacion)
        {
            try
            {
                using (SQLiteConnection connection = new(_configuration.GetConnectionString("SQLConnServidor")))
                {
                    connection.Open();

                    string query =
                        "SELECT " +
                            "id, " +
                            "nombre, " +
                            "apellidoPaterno, " +
                            "apellidoMaterno, " +
                            "identificacion " +
                        "FROM Persona " +
                        "WHERE identificacion = @identificacion ";

                    SQLiteCommand dbCommand = new(query, connection);
                    dbCommand.Parameters.Add(new SQLiteParameter("@identificacion", identificacion));
                    dbCommand.CommandType = CommandType.Text;

                    using (SQLiteDataReader objReader = dbCommand.ExecuteReader())
                    {
                        while (objReader.Read())
                        {
                            return new()
                            {
                                Id = Convert.ToInt32(objReader["id"]),
                                Nombre = objReader["nombre"].ToString(),
                                ApellidoPaterno = objReader["apellidoPaterno"].ToString(),
                                ApellidoMaterno = objReader["apellidoMaterno"].ToString(),
                                Identificacion = objReader["identificacion"].ToString()
                            };
                        }
                    }
                }

                return null;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw new ApplicationException(exp.Message, exp);
            }
        }

        public Persona FindPersonaById(int idPersona)
        {
            try
            {
                using (SQLiteConnection connection = new(_configuration.GetConnectionString("SQLConnServidor")))
                {
                    connection.Open();

                    string query =
                        "SELECT " +
                            "id, " +
                            "nombre, " +
                            "apellidoPaterno, " +
                            "apellidoMaterno, " +
                            "identificacion " +
                        "FROM Persona " +
                        "WHERE id = @id ";

                    SQLiteCommand dbCommand = new(query, connection);
                    dbCommand.Parameters.Add(new SQLiteParameter("@id", idPersona));
                    dbCommand.CommandType = CommandType.Text;

                    using (SQLiteDataReader objReader = dbCommand.ExecuteReader())
                    {
                        while (objReader.Read())
                        {
                            return new()
                            {
                                Id = Convert.ToInt32(objReader["id"]),
                                Nombre = objReader["nombre"].ToString(),
                                ApellidoPaterno = objReader["apellidoPaterno"].ToString(),
                                ApellidoMaterno = objReader["apellidoMaterno"].ToString(),
                                Identificacion = objReader["identificacion"].ToString()
                            };
                        }
                    }
                }

                return null;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw new ApplicationException(exp.Message, exp);
            }
        }

        public bool DeletePersonaByIdentificacion(string identificacion)
        {
            try
            {
                using (SQLiteConnection connection = new(_configuration.GetConnectionString("SQLConnServidor")))
                {
                    connection.Open();

                    string query =
                        "DELETE " +
                        "FROM Persona " +
                        "WHERE identificacion = @identificacion";

                    SQLiteCommand dbCommand = new(query, connection);
                    dbCommand.Parameters.Add(new SQLiteParameter("@identificacion", identificacion));
                    dbCommand.CommandType = CommandType.Text;

                    if(dbCommand.ExecuteNonQuery() < 1)
                        return false;

                    return true;
                }
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw new ApplicationException(exp.Message, exp);
            }
        }

        public bool StoredPersona(Persona persona)
        {
            try
            {
                using (SQLiteConnection connection = new(_configuration.GetConnectionString("SQLConnServidor")))
                {
                    connection.Open();

                    string query =
                        "INSERT INTO Persona (nombre,apellidoPaterno,apellidoMaterno,identificacion) " +
                        "VALUES(@nombre,@apellidoPaterno,@apellidoMaterno,@identificacion) ";

                    SQLiteCommand dbCommand = new(query, connection);
                    dbCommand.Parameters.Add(new SQLiteParameter("@nombre", persona.Nombre));
                    dbCommand.Parameters.Add(new SQLiteParameter("@apellidoPaterno", persona.ApellidoPaterno));
                    dbCommand.Parameters.Add(new SQLiteParameter("@apellidoMaterno", persona.ApellidoMaterno));
                    dbCommand.Parameters.Add(new SQLiteParameter("@identificacion", persona.Identificacion));
                    dbCommand.CommandType = CommandType.Text;

                    if (dbCommand.ExecuteNonQuery() < 1)
                        return false;

                    return true;
                }
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw new ApplicationException(exp.Message, exp);
            }
        }

        public bool ValidarExistenciaIdentificacion(string identificacion)
        {
            try
            {
                using (SQLiteConnection connection = new(_configuration.GetConnectionString("SQLConnServidor")))
                {
                    connection.Open();

                    string query =
                        "SELECT " +
                            "COUNT(1) Exist "+
                        "FROM Persona " +
                        "WHERE identificacion = @identificacion ";

                    SQLiteCommand dbCommand = new(query, connection);
                    dbCommand.Parameters.Add(new SQLiteParameter("@identificacion", identificacion));
                    dbCommand.CommandType = CommandType.Text;

                    using (SQLiteDataReader objReader = dbCommand.ExecuteReader())
                    {
                        while (objReader.Read())
                        {
                            if (Convert.ToInt32(objReader["Exist"]) > 0)
                                return true;
                        }
                    }
                }

                return false;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp.Message);
                throw new ApplicationException(exp.Message, exp);
            }
        }
    }
}
