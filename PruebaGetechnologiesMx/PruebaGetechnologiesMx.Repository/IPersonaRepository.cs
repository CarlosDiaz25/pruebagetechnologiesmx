﻿using PruebaGetechnologiesMx.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaGetechnologiesMx.Repository
{
    public interface IPersonaRepository
    {
        List<Persona> FindPersonas();
        Persona FindPersonaByIdentificacion(string identificacion);
        bool DeletePersonaByIdentificacion(string identificacion);
        bool StoredPersona(Persona persona);
        bool ValidarExistenciaIdentificacion(string identificacion);
        Persona FindPersonaById(int idPersona);
    }
}
