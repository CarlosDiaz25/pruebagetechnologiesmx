﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaFrontendGetech.Models
{
    public class ResponseWebApi
    {
        public int StatusCode { get; set; }
        public string? Message { get; set; }

        public string? StackTrace { get; set; }
    }
}
