﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PruebaFrontendGetech.Models
{
    [Serializable]
    [DataContract]
    public class Persona : PropertyChangedNotification
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id 
        {
            get { return GetValue(() => Id);}
            set { SetValue(() => Id, value);}
        }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "*")]
        [RegularExpression(@"^[A-Z][a-zA-Z ]*$", ErrorMessage = "Formato Incorrecto")]
        [MaxLength(20, ErrorMessage = "El Nombre excede los 20 caracteres.")]
        [DisplayName("nombre")]
        [DataMember(Name = "nombre")]
        public string Nombre 
        {
            get { return GetValue(() => Nombre); }
            set { SetValue(() => Nombre, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "*")]
        [RegularExpression(@"^[A-Z][a-zA-Z ]*$", ErrorMessage = "Formato Incorrecto")]
        [MaxLength(20, ErrorMessage = "El Apellido Paterno excede los 20 caracteres.")]
        [DisplayName("apellidoPaterno")]
        [DataMember(Name = "apellidoPaterno")]
        public string ApellidoPaterno 
        {
            get { return GetValue(() => ApellidoPaterno); }
            set { SetValue(() => ApellidoPaterno, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        [RegularExpression(@"^[A-Z][a-zA-Z ]*$", ErrorMessage = "Formato Incorrecto")]
        [MaxLength(20, ErrorMessage = "El Apellido Materno excede los 20 caracteres.")]
        [DisplayName("apellidoMaterno")]
        [DataMember(Name = "apellidoMaterno")]
        public string ApellidoMaterno 
        {
            get { return GetValue(() => ApellidoMaterno); }
            set { SetValue(() => ApellidoMaterno, value); }
        }

        /// <summary>
        /// Identificacion oficial de la INE
        /// </summary>
        ///  [Required(ErrorMessage = "*")]
        ///  ^[1-9]+$
        [RegularExpression(@"^[1-9]+$", ErrorMessage = "Formato Incorrecto")]
        [Required(ErrorMessage = "*")]
        [DisplayName("identificacion")]
        [DataMember(Name = "identificacion")]
        public string Identificacion 
        {
            get { return GetValue(() => Identificacion); }
            set { SetValue(() => Identificacion, value); }
        }

    }
}
