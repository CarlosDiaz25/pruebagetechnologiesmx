﻿using Newtonsoft.Json;
using Prism.Commands;
using PruebaFrontendGetech.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Xml;
using static System.Net.WebRequestMethods;

namespace PruebaFrontendGetech.ViewModels
{

    public class UsuarioViewModel : PropertyChangedNotification
    {
        private readonly static string _baseUrl = "https://localhost:44329/api/DirectorioRestService/";
        private static RestClient _client;
        private static RestRequest _request;

        #region Comandos
        public ICommand Guardar { get; private set; }
        #endregion

        public Persona persona
        {
            get { return GetValue(() => persona); }
            set { SetValue(() => persona, value); }
        }

        public UsuarioViewModel()
        {
            try
            {
                Guardar = new DelegateCommand(OnGuardarPersona);
                persona = new Persona();
            }
            catch (Exception ex)
            {
                MessageBox.Show("UsuarioViewModel" + Environment.NewLine + ex.Message);
            }
        }

        private void OnGuardarPersona()
        {
            try
            {
                if (!DatosValidos())
                    return;

                _client = new RestClient(_baseUrl);
                _request = new RestRequest("storedPersona", Method.Post);
                _request.AddHeader("Content-Type", "application/json");
                _request.AddParameter("application/json", JsonConvert.SerializeObject(persona), ParameterType.RequestBody);
                var response = _client.Execute(_request);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    ResponseWebApi responseWeb = JsonConvert.DeserializeObject<ResponseWebApi>(response.Content);
                    MessageBox.Show(responseWeb.Message);
                    return;
                }

                MessageBox.Show("Proceso Generado con exito");
                persona = new Persona();

            }
            catch (Exception ex)
            {
                MessageBox.Show("OnGuardarPersona" + Environment.NewLine + ex.Message);
            }
        }

        private bool DatosValidos()
        {
            try
            {
                if (string.IsNullOrEmpty(persona.Nombre))
                {
                    MessageBox.Show("Favor de ingresar un nombre");
                    return false;
                }

                if (string.IsNullOrEmpty(persona.ApellidoPaterno))
                {
                    MessageBox.Show("Favor de ingresar un apellido paterno");
                    return false;
                }

                if (string.IsNullOrEmpty(persona.Identificacion))
                {
                    MessageBox.Show("Favor de ingresar una identificacion");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("DatosValidos" + Environment.NewLine + ex.Message);
                return false;
            }
        }


    }
}
