﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;

namespace PruebaFrontendGetech
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(
                            System.Windows.Markup.XmlLanguage.GetLanguage(
                            System.Globalization.CultureInfo.CurrentCulture.IetfLanguageTag)));

              
                Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                Views.UsuarioView logon = new Views.UsuarioView();
                bool? res = logon.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
